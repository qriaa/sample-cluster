package com.example;

import com.rabbitmq.client.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class LoggerMicroservice {

    static DateFormat fullDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
    static String EXCHANGE_NAME = "LOGGER";
    static BufferedWriter writer;
    static DefaultConsumer consumer;

    private static void logMessageToFile(String msg) {
        try {
            writer.write(msg + "\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void logMessageToConsole(String msg) {
        System.out.println(msg);
    }

    public static void consumeMessage(String msg) {
        String logMsg = String.format("[%s] %s", fullDateFormat.format(new Date()), msg);
        logMessageToConsole(logMsg);
        logMessageToFile(logMsg);
    }

    public static void main(String[] args) throws IOException, TimeoutException {
        // make connection
        ConnectionFactory connFact = new ConnectionFactory();
        connFact.setHost("rabbitmq");
        Connection connection;
        Channel channel;

        connection = connFact.newConnection();
        channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        ArrayList<String> queues = new ArrayList<>();
        for (String routingKey : args) {
            String queue = channel.queueDeclare().getQueue();
            queues.add(queue);
            channel.queueBind(queue, EXCHANGE_NAME, routingKey);
        }

        // init writer
        String logFilename = "RSI_Projekt.";
        for (String arg : args) {
            logFilename += arg + ".";
        }
        logFilename += String.format("%s.log", fullDateFormat.format(new Date()));
        File logFile = new File(logFilename);

        writer = Files.newBufferedWriter(logFile.toPath());

        // enable consumption
        consumer = new DefaultConsumer(channel) {
            public void handleDelivery(String consumerTag,
                                       Envelope envelope,
                                       AMQP.BasicProperties properties,
                                       byte[] body) {
                String msg = new String(body, StandardCharsets.UTF_8);
                LoggerMicroservice.consumeMessage(msg);
            }
        };

        for (String queue : queues) {
            channel.basicConsume(queue, true, consumer);
        }

        // is operational now
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    channel.close();
                    connection.close();
                    writer.close();
                } catch (IOException | TimeoutException ignored) {

                }
            }
        });

        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        }
    }
}