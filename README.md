# Sample-cluster
## About
Sample cluster is a project designed to show how one could self-manage a kubernetes cluster in the cloud (in this case - Azure). The cloud is managed by Terraform in order to achieve the benefits of IaC such as consistency and reduction of manual configuration. Also, Ansible is used to ensure the ease of scalability and self-documentation of configuration. Additionally a Gitlab CI pipeline is implemented in order to automate testing, integration and deployment.

## Technologies used
 - Kubernetes
 - Docker
 - Ansible
 - Terraform
 - nginx
 - Spring
 - RabbitMQ

## Architecture
Azure resources are provisioned by Terraform, and the configuration is managed by Ansible.  
![Sample-cluster Azure architecture](docs/azure-diag.jpeg)  
Nginx provides a frontend to the application, whereas a Spring Boot application implements a Rest API. The requests are logged in logger-admin and logger-sales. Communication is achieved by a RabbitMQ Broker.  
![Sample-cluster application architecture](docs/application-diag.jpg)  
