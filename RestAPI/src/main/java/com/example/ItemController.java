package com.example;

import net.minidev.json.JSONObject;
import org.springframework.hateoas.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin
@RestController
public class ItemController {
    private RabbitMQManager rabbitmqManager;

    private ItemEntryRepository dataRepo = new ItemEntryRepositoryImpl();

    ItemController(){
        try {
            rabbitmqManager = new RabbitMQManager();
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/items/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityModel<ItemEntry> getItemEntry(@PathVariable int id) {
        try {
            rabbitmqManager.log("GET /items/" + id, RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            ItemEntry result = dataRepo.getItemEntry(id);
            return getEModelFromEntry(result);
        } catch (ItemEntryNotFoundEx e) {
            rabbitmqManager.log("GET /items/" + id + " EXCEPTION", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            throw e;
        }
    }

    @GetMapping("/items")
    @ResponseStatus(HttpStatus.OK)
    public CollectionModel<EntityModel<ItemEntry>> getItemEntries() {
        rabbitmqManager.log("GET /items", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
        List<EntityModel<ItemEntry>> entries = dataRepo.getAllItemEntries().stream().map(
                entry -> getEModelFromEntry(entry)
        ).collect(Collectors.toList());
        return CollectionModel.of(entries, linkTo(methodOn(ItemController.class).getItemEntries()).withSelfRel());
    }

    @DeleteMapping("/items/{id}")
    public EntityModel<ResponseEntity<?>> deleteItemEntry(@PathVariable int id) {
        try {
            rabbitmqManager.log("DELETE /items/" + id, RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            dataRepo.deleteItemEntry(id);
            return EntityModel.of(ResponseEntity.noContent().build(),
                    linkTo(methodOn(ItemController.class).getItemEntries()).withRel("getAll"));
        } catch (ItemEntryNotFoundEx e) {
            rabbitmqManager.log("DELETE /items/" + id + " EXCEPTION", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            throw e;
        }
    }

    @PostMapping("/items")
    @ResponseStatus(HttpStatus.CREATED)
    public EntityModel<ItemEntry> addItemEntry(@RequestBody ItemEntry entry) {
        try {
            rabbitmqManager.log("POST /items", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            ItemEntry result = dataRepo.addItemEntry(entry.getId(), entry.getName(), entry.getUnitPrice(), entry.getAmount(), entry.isAvailable());
            return getEModelFromEntry(result);
        } catch (ItemEntryExistsEx e) {
            rabbitmqManager.log("POST /items EXCEPTION", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            throw e;
        }
    }

    @PutMapping("/items")
    public ResponseEntity<ItemEntry> updateItemEntry(@RequestBody ItemEntry entry) {
        try {
            rabbitmqManager.log("PUT /items", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            ItemEntry result = dataRepo.updateItemEntry(entry.getId(), entry.getName(), entry.getUnitPrice(), entry.getAmount(), entry.isAvailable());
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .body(result);
        } catch (ItemEntryNotFoundEx e) {
            rabbitmqManager.log("PUT /items NOT FOUND EXCEPTION - ADDING", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            ItemEntry result = dataRepo.addItemEntry(entry.getId(), entry.getName(), entry.getUnitPrice(), entry.getAmount(), entry.isAvailable());
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .body(result);
        }
    }

    @PatchMapping("/items/buy/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityModel<JSONObject> buyItem(@PathVariable int id) {
        rabbitmqManager.log("PATCH /items/buy/" + id, RabbitMQManager.LOG_ROUTING_KEY.SALES_AND_SUPPLIES);
        try{
            String result = dataRepo.buyItem(id);
            JSONObject jsonObject = new JSONObject();
            jsonObject.appendField("content", result);
            EntityModel<JSONObject> emodel = EntityModel.of(jsonObject);
            emodel.add(linkTo(methodOn(ItemController.class).getItemEntry(id)).withSelfRel());
            emodel.add(linkTo(methodOn(ItemController.class).getItemEntries()).withRel("getAll"));
            emodel.add(linkTo(methodOn(ItemController.class).deleteItemEntry(id)).withRel("delete"));
            if(dataRepo.getItemEntry(id).isAvailable()) {
                emodel.add(linkTo(methodOn(ItemController.class).buyItem(id)).withRel("buy"));
            }
            return emodel;
        } catch (ItemEntryNotFoundEx e) {
            rabbitmqManager.log("PATCH /items/buy/" + id + " NOT FOUND EXCEPTION", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            throw e;
        } catch (ItemEntryNotAvailableEx e) {
            rabbitmqManager.log("PATCH /items/buy/" + id + " NOT AVAILABLE EXCEPTION" + id, RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            throw e;
        }
    }

    @PatchMapping("/items/supply/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ItemEntry supplyItem(@PathVariable int id) {
        rabbitmqManager.log("PATCH /items/supply/" + id, RabbitMQManager.LOG_ROUTING_KEY.SALES_AND_SUPPLIES);
        try {
            ItemEntry result = dataRepo.supplyItem(id);
            return result;
        } catch (ItemEntryNotFoundEx e) {
            rabbitmqManager.log("PATCH /items/supply/" + id + " NOT FOUND EXCEPTION", RabbitMQManager.LOG_ROUTING_KEY.ADMIN);
            System.out.println();
            throw e;
        }
    }

    private EntityModel<ItemEntry> getEModelFromEntry(ItemEntry entry) {
        EntityModel<ItemEntry> emodel = EntityModel.of(entry);
        emodel.add(linkTo(methodOn(ItemController.class).getItemEntry(entry.getId())).withSelfRel());
        emodel.add(linkTo(methodOn(ItemController.class).getItemEntries()).withRel("getAll"));
        emodel.add(linkTo(methodOn(ItemController.class).deleteItemEntry(entry.getId())).withRel("delete"));
        if(entry.isAvailable()) {
            emodel.add(linkTo(methodOn(ItemController.class).buyItem(entry.getId())).withRel("buy"));
        }
        emodel.add(linkTo(methodOn(ItemController.class).supplyItem(entry.getId())).withRel("supply"));
        return emodel;

    }
}
