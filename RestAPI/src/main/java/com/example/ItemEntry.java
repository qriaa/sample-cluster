package com.example;

//@RestController
//@GetMapping("/blabla/{id}")
//@DeleteMapping(value="/persons/{id}", method = RequestMethod.GET)
public class ItemEntry {
    private int id;
    private String name;
    private float unitPrice;
    private int amount;
    private boolean isAvailable;

    public ItemEntry() {
    }

    public ItemEntry(int id, String name, float unitPrice, int amount, boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.amount = amount;
        this.isAvailable = isAvailable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}
