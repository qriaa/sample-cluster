package com.example;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class ItemEntryExistsEx extends RuntimeException {
    public ItemEntryExistsEx() {
        super("This entry already exists");
    }

    public ItemEntryExistsEx(int id) {
        super("The entry of id = " + id + " already exists");
    }
}
