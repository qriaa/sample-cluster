package com.example;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class ItemEntryNotAvailableEx extends RuntimeException {
    public ItemEntryNotAvailableEx() {
        super("This entry is not available");
    }

    public ItemEntryNotAvailableEx(int id) {
        super("The entry of id = " + id + " is not available");
    }
}
