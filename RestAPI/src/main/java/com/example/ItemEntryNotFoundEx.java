package com.example;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ItemEntryNotFoundEx extends RuntimeException {
    public ItemEntryNotFoundEx() {
        super("The specified entry does not exist");
    }
    public ItemEntryNotFoundEx(int id) {
        super("The entry of id = " + id + " does not exist");
    }
}
