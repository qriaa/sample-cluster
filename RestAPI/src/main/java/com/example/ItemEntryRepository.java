package com.example;

import java.util.List;

public interface ItemEntryRepository {
    List<ItemEntry> getAllItemEntries();
    ItemEntry getItemEntry(int id) throws ItemEntryNotFoundEx;
    ItemEntry updateItemEntry(int id, String name, float unitPrice, int amount, boolean isAvailable) throws ItemEntryNotFoundEx;
    boolean deleteItemEntry(int id) throws ItemEntryNotFoundEx;
    ItemEntry addItemEntry(int id, String name, float unitPrice, int amount, boolean isAvailable) throws ItemEntryExistsEx;
    String buyItem(int id) throws ItemEntryNotFoundEx, ItemEntryNotAvailableEx;
    ItemEntry supplyItem(int id) throws ItemEntryNotFoundEx;
    int countItemEntries();
}

