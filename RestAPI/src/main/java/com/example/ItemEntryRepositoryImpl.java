package com.example;

import java.util.ArrayList;
import java.util.List;

public class ItemEntryRepositoryImpl implements ItemEntryRepository {
    private List<ItemEntry> itemEntryList;
    public ItemEntryRepositoryImpl() {
        itemEntryList = new ArrayList<>();
        itemEntryList.add(new ItemEntry(1, "Kamera Sony alpha", 1000.0f, 1, true));
        itemEntryList.add(new ItemEntry(2, "Soczewka 500mm", 600.0f, 3, true));
        itemEntryList.add(new ItemEntry(3, "Soczewka 80mm", 400.0f, 2, true));
        itemEntryList.add(new ItemEntry(4, "Kamera Canon", 1200.0f, 1, true));
    }
    public List<ItemEntry> getAllItemEntries() {
        return itemEntryList;
    }
    public ItemEntry getItemEntry(int id) throws ItemEntryNotFoundEx {
        for (ItemEntry entry : itemEntryList) {
            if (entry.getId() == id) {
                return entry;
            }
        }
        throw new ItemEntryNotFoundEx(id);
    }
    public ItemEntry addItemEntry(int id, String name, float unitPrice, int amount, boolean isAvailable) throws ItemEntryExistsEx {
        for (ItemEntry entry : itemEntryList) {
            if (entry.getId() == id) {
                throw new ItemEntryExistsEx(id);
            }
        }
        ItemEntry entry = new ItemEntry(id, name, unitPrice, amount, isAvailable);
        itemEntryList.add(entry);
        return entry;
    }

    @Override
    public String buyItem(int id) throws ItemEntryNotFoundEx, ItemEntryNotAvailableEx {
        for (ItemEntry entry : itemEntryList) {
            if (entry.getId() == id) {
                if(!entry.isAvailable()) {
                    throw new ItemEntryNotAvailableEx(id);
                }
                entry.setAmount(entry.getAmount()-1);
                if (entry.getAmount() <= 0) {
                    entry.setAvailable(false);
                }
                return "You bought " + entry.getName() + " for the price of " + entry.getUnitPrice();

            }
        }
        throw new ItemEntryNotFoundEx(id);
    }

    @Override
    public ItemEntry supplyItem(int id) throws ItemEntryNotFoundEx {
        for (ItemEntry entry : itemEntryList) {
            if (entry.getId() == id) {
                entry.setAmount(entry.getAmount()+1);
                entry.setAvailable(true);
                return entry;
            }
        }
        throw new ItemEntryNotFoundEx(id);
    }

    public boolean deleteItemEntry(int id) throws ItemEntryNotFoundEx {
        for (ItemEntry entry : itemEntryList) {
            if (entry.getId() == id) {
                itemEntryList.remove(entry);
                return true;
            }
        }
        throw new ItemEntryNotFoundEx(id);
    }
    public ItemEntry updateItemEntry(int id, String name, float unitPrice, int amount, boolean isAvailable) throws ItemEntryNotFoundEx {
        for (ItemEntry entry : itemEntryList) {
            if (entry.getId() == id) {
                entry.setName(name);
                entry.setUnitPrice(unitPrice);
                entry.setAmount(amount);
                entry.setAvailable(isAvailable);
                return entry;
            }
        }
        throw new ItemEntryNotFoundEx();
    }
    public int countItemEntries() {
        return itemEntryList.size();
    }
}
