package com.example;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

public class RPCClient {

    String correlationID;
    public Connection connection;
    public Channel channel;
    String replyQueueName;

    RPCClient() throws IOException, TimeoutException {
        correlationID = UUID.randomUUID().toString();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();

        replyQueueName = channel.queueDeclare().getQueue();

        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                .correlationId(correlationID)
                .replyTo(replyQueueName)
                .build();

    }

}
