package com.example;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;



public class RabbitMQManager {

    public enum EXCHANGE {
        LOGGER
    }
    public enum LOG_ROUTING_KEY {
        ADMIN,
        SALES_AND_SUPPLIES
    }
    public Connection connection;
    public Channel channel;

    RabbitMQManager() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        connection = factory.newConnection();
        channel = connection.createChannel();
        initLoggers();
    }

    void initLoggers() throws IOException {
        channel.exchangeDeclare(EXCHANGE.LOGGER.name(), BuiltinExchangeType.DIRECT);
    }

    void log(String msg, LOG_ROUTING_KEY key) {
        try {
            channel.basicPublish(EXCHANGE.LOGGER.name(), key.name(), null, msg.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
