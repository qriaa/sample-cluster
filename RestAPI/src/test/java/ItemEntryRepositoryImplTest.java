import com.example.ItemEntryNotAvailableEx;
import com.example.ItemEntryNotFoundEx;
import com.example.ItemEntryRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ItemEntryRepositoryImplTest {

    ItemEntryRepositoryImpl repository;
    @BeforeEach
    void recreateRepository() {
        repository = new ItemEntryRepositoryImpl();
    }

    @Test
    void buyItem_BoughtOne_Success() {
        try {
            String result = repository.buyItem(1);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void buyItem_BoughtTooMuch_NotAvailableException() {
        repository.buyItem(1);
        assertThrowsExactly(ItemEntryNotAvailableEx.class, () -> repository.buyItem(1));
    }

    @Test
    void buyItem_BoughtNonExisting_NotFoundException() {
        assertThrowsExactly(ItemEntryNotFoundEx.class, () -> repository.buyItem(-99999));
    }
}
