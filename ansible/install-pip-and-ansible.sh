#!/bin/sh
#Ensure pip and ansible installed on system
pip --version;
if [ $? -ne 0 ]; then
    sudo apt update;
    sudo apt install -y python3-pip;
fi;

ansible --version;
if [ $? -ne 0 ]; then
    python3 -m pip install --user ansible;
    if [ $? -ne 0 ]; then
        echo "export PATH=\"\$HOME/.local/bin:\$PATH\"" >> ~/.bashrc;
    fi;
fi;