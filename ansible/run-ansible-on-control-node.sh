#!/bin/sh
export ANSIBLE_HOST_KEY_CHECKING=False;

# gain permissions
eval $(ssh-agent -s);
chmod 600 ~/adminman-key;
base64 -d < ~/adminman-key | ssh-add - > /dev/null;

ansible-playbook -i inventory.yaml install-k8s.yaml;
ANSIBLE_ERROR_CODE=$?;
unset ANSIBLE_HOST_KEY_CHECKING;
eval $(ssh-agent -k);
exit $ANSIBLE_ERROR_CODE;