FROM eclipse-temurin:19 AS builder
COPY . /home/sample-cluster/
WORKDIR /home/sample-cluster/
RUN ./mvnw dependency:resolve
CMD [ "/bin/bash" ]