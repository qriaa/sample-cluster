FROM qriaa/sample-cluster-dependencies as builder
RUN ./mvnw install -pl :LoggerMicroservice -am

FROM eclipse-temurin:19-jre-alpine
WORKDIR /home/sample-cluster/
COPY --from=builder /home/sample-cluster/LoggerMicroservice/target/LoggerMicroservice.jar .
ENTRYPOINT [ "java", "-jar", "LoggerMicroservice.jar" ]
CMD [ "ADMIN" ]