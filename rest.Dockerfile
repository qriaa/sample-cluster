FROM qriaa/sample-cluster-dependencies as builder
RUN ./mvnw install -pl :RestAPI -am

FROM eclipse-temurin:19-jre-alpine
WORKDIR /home/sample-cluster/
COPY --from=builder /home/sample-cluster/RestAPI/target/RestAPI.jar .
ENTRYPOINT [ "java", "-jar", "RestAPI.jar" ]
CMD [ "" ]