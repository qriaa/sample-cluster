#!/bin/bash
terraform apply -auto-approve -destroy --target="azurerm_linux_virtual_machine.sample-cluster-k8s-cp-1" && \
terraform apply -auto-approve -destroy --target="azurerm_linux_virtual_machine.sample-cluster-k8s-wn-1" && \
terraform apply -auto-approve;
