terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "sample-cluster" {
  name     = "sample-cluster"
  location = "East US"
  tags = {
    env = "dev"
  }
}

resource "azurerm_virtual_network" "sample-cluster-vn" {
  name                = "sample-cluster-vn"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
  address_space       = ["10.0.0.0/16"]
  tags = {
    env = "dev"
  }
}

resource "azurerm_subnet" "sample-cluster-subnet-1" {
  name                 = "sample-cluster-subnet-1"
  resource_group_name  = azurerm_resource_group.sample-cluster.name
  virtual_network_name = azurerm_virtual_network.sample-cluster-vn.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_security_group" "sample-cluster-nsg" {
  name                = "sample-cluster-nsg"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
}

resource "azurerm_network_security_rule" "sample-cluster-nsg-ssh" {
  name                        = "ssh"
  resource_group_name         = azurerm_resource_group.sample-cluster.name
  network_security_group_name = azurerm_network_security_group.sample-cluster-nsg.name

  priority                   = 100
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "22"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
}

resource "azurerm_public_ip" "sample-cluster-ansible-control-public-ip" {
  name                = "sample-cluster-ansible-control-public-ip"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location

  allocation_method = "Dynamic"
  domain_name_label = "ansible-sample-cluster"
}

resource "azurerm_network_interface" "sample-cluster-ansible-control-ni" {
  name                = "sample-cluster-ansible-control-ni"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.sample-cluster-subnet-1.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.priv-ip-ansible
    public_ip_address_id          = azurerm_public_ip.sample-cluster-ansible-control-public-ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "sample-cluster-ansible-control-ni-nsg" {
  network_interface_id      = azurerm_network_interface.sample-cluster-ansible-control-ni.id
  network_security_group_id = azurerm_network_security_group.sample-cluster-nsg.id
}

resource "azurerm_linux_virtual_machine" "sample-cluster-ansible-control" {
  name                = "sample-cluster-ansible-control"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
  size                = "Standard_B1ls"
  network_interface_ids = [
    azurerm_network_interface.sample-cluster-ansible-control-ni.id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  admin_username = "adminman"
  admin_ssh_key {
    username   = "adminman"
    public_key = file("~/.ssh/azure-sample-cluster.pub")
  }
}

resource "azurerm_network_security_rule" "sample-cluster-nsg-kubernetes" {
  name                        = "kubernetes"
  resource_group_name         = azurerm_resource_group.sample-cluster.name
  network_security_group_name = azurerm_network_security_group.sample-cluster-nsg.name

  priority                   = 200
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "6443"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
}

resource "azurerm_public_ip" "sample-cluster-k8s-cp-1-public-ip" {
  name                = "sample-cluster-k8s-cp-1-public-ip"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location

  allocation_method = "Dynamic"
  domain_name_label = "kubernetes-control-plane"
}


resource "azurerm_network_interface" "sample-cluster-k8s-cp-ni-1" {
  name                = "sample-cluster-k8s-cp-ni-1"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.sample-cluster-subnet-1.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.priv-ip-k8s-cp-1
    public_ip_address_id          = azurerm_public_ip.sample-cluster-k8s-cp-1-public-ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "sample-cluster-k8s-cp-1-ni-nsg" {
  network_interface_id      = azurerm_network_interface.sample-cluster-k8s-cp-ni-1.id
  network_security_group_id = azurerm_network_security_group.sample-cluster-nsg.id
}

resource "azurerm_linux_virtual_machine" "sample-cluster-k8s-cp-1" {
  name                = "sample-cluster-k8s-cp-1"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
  size                = "Standard_B2s"
  network_interface_ids = [
    azurerm_network_interface.sample-cluster-k8s-cp-ni-1.id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  admin_username = "adminman"
  admin_ssh_key {
    username   = "adminman"
    public_key = file("~/.ssh/azure-sample-cluster.pub")
  }
}

resource "azurerm_network_security_rule" "sample-cluster-nsg-restapi" {
  name                        = "restapi"
  resource_group_name         = azurerm_resource_group.sample-cluster.name
  network_security_group_name = azurerm_network_security_group.sample-cluster-nsg.name

  priority                   = 300
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "30000"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
}

resource "azurerm_network_security_rule" "sample-cluster-nsg-frontend" {
  name                        = "frontend"
  resource_group_name         = azurerm_resource_group.sample-cluster.name
  network_security_group_name = azurerm_network_security_group.sample-cluster-nsg.name

  priority                   = 400
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "30001"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
}

resource "azurerm_network_interface" "sample-cluster-k8s-wn-ni-1" {
  name                = "sample-cluster-k8s-wn-ni-1"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.sample-cluster-subnet-1.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.priv-ip-k8s-wn-1
  }
}

resource "azurerm_network_interface_security_group_association" "sample-cluster-k8s-wn-1" {
  network_interface_id      = azurerm_network_interface.sample-cluster-k8s-wn-ni-1.id
  network_security_group_id = azurerm_network_security_group.sample-cluster-nsg.id
}

resource "azurerm_linux_virtual_machine" "sample-cluster-k8s-wn-1" {
  name                = "sample-cluster-k8s-wn-1"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
  size                = "Standard_B1ms"
  network_interface_ids = [
    azurerm_network_interface.sample-cluster-k8s-wn-ni-1.id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  admin_username = "adminman"
  admin_ssh_key {
    username   = "adminman"
    public_key = file("~/.ssh/azure-sample-cluster.pub")
  }
}


resource "azurerm_public_ip" "sample-cluster-api-lb-ip" {
  name                = "sample-cluster-api-lb-ip"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
  sku                 = "Standard"

  allocation_method = "Static"
  domain_name_label = "sample-cluster-restapi"
}

resource "azurerm_lb" "sample-cluster-api-lb" {
  name                = "sample-cluster-api-lb"
  resource_group_name = azurerm_resource_group.sample-cluster.name
  location            = azurerm_resource_group.sample-cluster.location
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "sample-cluster-api-lb-frontend-ip"
    public_ip_address_id = azurerm_public_ip.sample-cluster-api-lb-ip.id
  }
}

resource "azurerm_lb_backend_address_pool" "sample-cluster-api-lb-backend-pool" {
  name            = "sample-cluster-api-lb-backend-pool"
  loadbalancer_id = azurerm_lb.sample-cluster-api-lb.id
}

resource "azurerm_lb_backend_address_pool_address" "sample-cluster-api-lb-backend-pool-wn1" {
  name                    = "sample-cluster-api-lb-backend-pool-wn1"
  backend_address_pool_id = azurerm_lb_backend_address_pool.sample-cluster-api-lb-backend-pool.id
  virtual_network_id      = azurerm_virtual_network.sample-cluster-vn.id
  ip_address              = "10.0.1.120"
}

resource "azurerm_lb_rule" "sample-cluster-api-lb-rule" {
  loadbalancer_id                = azurerm_lb.sample-cluster-api-lb.id
  name                           = "sample-cluster-api-lb-rule"
  protocol                       = "Tcp"
  frontend_port                  = 8080
  backend_port                   = 30000
  frontend_ip_configuration_name = azurerm_lb.sample-cluster-api-lb.frontend_ip_configuration[0].name
  backend_address_pool_ids = [ azurerm_lb_backend_address_pool.sample-cluster-api-lb-backend-pool.id ]
}

resource "azurerm_lb_rule" "sample-cluster-api-lb-frontend-rule" {
  loadbalancer_id                = azurerm_lb.sample-cluster-api-lb.id
  name                           = "sample-cluster-api-lb-frontend-rule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 30001
  frontend_ip_configuration_name = azurerm_lb.sample-cluster-api-lb.frontend_ip_configuration[0].name
  backend_address_pool_ids = [ azurerm_lb_backend_address_pool.sample-cluster-api-lb-backend-pool.id ]
}