variable "priv-ip-ansible" {
  type    = string
  default = "10.0.1.100"
}

variable "priv-ip-k8s-cp-1" {
  type    = string
  default = "10.0.1.110"
}

variable "priv-ip-k8s-wn-1" {
  type    = string
  default = "10.0.1.120"
}

variable "priv-ip-k8s-wn-2" {
  type    = string
  default = "10.0.1.121"
}